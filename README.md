# dns-monitor

A DNS change monitoring tool written in PHP, which can output HTML reports designed for sending via email.

## Changelog

| Date | Notes |
|------|-------|
| **2020-06-11** | `dns-records.json` is no longer automatically updated when generating a debug report, adjusted line breaks to make reports clearer, reduced cooldown from 4 hours to 2 hours. |
| **2020-03-25** | Fixed bug where debug reports would still trigger a cooldown. Adjusted report styling to improve readability. |
| **2020-03-24** | Fixed missing `break;` in Switch/Case statement causing NS lookups to fail. |
| **2019-10-29** | `dns-records.json` is now automatically updated when generating a report. Zones to monitor are now specified as text files in the `zones/` directory. |
| **2019-10-28** | Switched config syntax to a 'pseudo-bind' style "host record match" (space-separated), removed requirement to manually specify DNS record type (E.g. 'DNS_A') and resource name (e.g. 'ip'). |
| **2019-06-26** | Fixed bug where the order of the DNS responses could cause a false-positive change alert. |
| **2019-02-26** | Initial release. |
| **2019-02-07** | Development started. |

## Command-line arguments

* ```update```: Update the list of expected DNS records (e.g. to be used if there is a legitimate change in DNS that you want to add to the program database)
* ```report standard [nocooldown]```: Output a standard report, optionally bypassing the cooldown period
* ```report debug```: Output a debug report, automatically bypassing the cooldown check

## Update Functionality

The update option will update the list of expected DNS records, which is useful when initially setting up the program and also to adopt new changes when legitimate DNS changes have been made.

## Report Functionality

The report fucntionality will output a basic HTML report designed for sending via email. This program does not handle the sending of the email itself - you should use an external program for this (e.g. sendmail in a bash script, Python with smtplib, etc).

## Program Setup

...WIP...

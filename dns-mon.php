<?php set_time_limit(10); date_default_timezone_set("Europe/London");

//Load zone(s) to monitor
$zoneFiles = scandir("zones", 0);
$targets = array();
foreach($zoneFiles as $fileName) {
  if($fileName[0] === ".") {
    continue;
  }
  $file = file("zones/" . $fileName);
  $targets = array_merge($targets, $file);
  fwrite(STDERR, "Found zone '" . $fileName . "' (containing " . count($file) . " records)\n");
}
$targets = array_map('str_getcsv_bind', $targets);

//Function to parse each line of the zone file as a CSV row
function str_getcsv_bind($line) {
  //Parse lines as CSV rows with a space as the delimeter
  $csvRow = str_getcsv($line, " ");
  //Construct associative array
  $target["host"] = $csvRow[0];
  $target["canonical_name"] = $csvRow[1];
  //Skip if match isn't present
  if(isset($csvRow[2])) {
    $target["match"] = $csvRow[2];
  } else {
    $target["match"] = ".*";
  }
  return $target;
}

//Load expected results
$expected = json_decode(file_get_contents("dns-records.json"), true);

//Acquire DNS record by host and type
function getDns($target, $type, $resourceName, $canonicalName, $match) {
  $result = array(
    $canonicalName => array()
  );
  $response = dns_get_record($target, $type);
  foreach($response as $record) {
    if(preg_match("/" . $match . "/i", $record[$resourceName])) {
      array_push($result[$canonicalName], $record[$resourceName]);
    }
  }
  sort($result[$canonicalName]);
  return($result);
}

//Get DNS records for all targets
function getRecords($targets, $writeRecords = 1) {
  $results = array();
  foreach($targets as $target => $params) {
    #Set resource_name and canonical_name
    switch($params["canonical_name"]) {
      case "A":
        $params["type"] = DNS_A;
        $params["resource_name"] = "ip";
        break;
      case "AAAA":
        $params["type"] = DNS_AAAA;
        $params["resource_name"] = "ipv6";
        break;
      case "CNAME":
        $params["type"] = DNS_CNAME;
        $params["resource_name"] = "target";
        break;
      case "MX":
        $params["type"] = DNS_MX;
        $params["resource_name"] = "target";
        break;
      case "TXT":
        $params["type"] = DNS_TXT;
        $params["resource_name"] = "txt";
        break;
      case "CAA":
        $params["type"] = DNS_CAA;
        $params["resource_name"] = "value";
        break;
      case "NS":
        $params["type"] = DNS_NS;
        $params["resource_name"] = "target";
        break;
      case "PTR":
        $params["type"] = DNS_PTR;
        $params["resource_name"] = "target";
        break;
      default:
        exit("Invalid DNS Record Type: '" . $params["canonical_name"] . "'\n");
    }
    fwrite(STDERR, "Checking '" . $params["host"] . " " . $params["canonical_name"] . "'...");
    if(!(isset($results[$params["host"]]))) {
      $results[$params["host"]][$params["canonical_name"]] = array();
    }
    $results[$params["host"]] = array_merge(
      $results[$params["host"]],
      getDns($params["host"], $params["type"], $params["resource_name"], $params["canonical_name"], $params["match"])
    );
    
    fwrite(STDERR, " Done\n");
    sleep(0.5);
  }
  if($writeRecords) {
    fwrite(STDERR, "Writing to dns-records.json...");
    file_put_contents("dns-records.json", json_encode($results));
    fwrite(STDERR, " Done\n");
  } else {
    fwrite(STDERR, "Not writing to dns-records.json as running in debug mode.\n");
  }
  return($results);
}

//Generate a diff between the current and expected DNS records
function generateDiff($results, $expected) {
  $body = "";
  foreach($results as $host => $records) {
    foreach($records as $record => $value) {
      if(!($value === $expected[$host][$record])) {
        $body .= "<p><b><u>" . $host . "</u></b> -> <b><u>" . $record . "</u></b> has changed...<br/>\n<b>New value(s):</b><br/>\n";
        foreach($value as $i) {
          $body .= "<code>" . $i . "</code><br/>\n";
        }
        $body .= "<b>Old value(s):</b><br/>\n";
        if($expected[$host][$record]) {
          foreach($expected[$host][$record] as $i) {
            $body .= "<code>" . $i . "</code><br/>\n";
          }
        } else {
          $body .= "<br/>NONE - New record?\n";
        }
        $body .= "</p>\n";
      }
    }
  }
  return($body);
}

//Handle cooldown period
function cooldown($action) {
  global $argv;
  if($argv[2] === "debug" || ((isset($argv[3])) && ($argv[3] === "nocooldown"))) {
    return;
  } elseif($action === "check") {
    $cooldownTime = rtrim(file_get_contents("cooldown.txt"));
    if(strtotime($cooldownTime) > strtotime("-2 hours")) {
      exit("Currently in 2 hour cooldown period (since " . $cooldownTime . ").\n");
    }
  } else {
    file_put_contents("cooldown.txt", date("Y-m-d g:i:s A"));
  }
}

//Generate HTML report in email format
function generateReport($targets, $expected, $to, $reportType, $sendIfEmpty, $writeRecords) {
  $now = date("Y-m-d g:i:s A");
  $headers = "From: CYC DNS Monitor <dns-alerts@york.gov.uk>
To: " . $to . "
Subject: " . $reportType . "DNS Change Report for " . $now . "
Content-Type: text/html
Content-Disposition: inline";
  $body = generateDiff(getRecords($targets, $writeRecords), $expected);
  $emptyMessage = "No DNS changes were detected at " . $now . ".";
  $bodyEmpty = 0;
  if(!($body)) {
    $body = "<p>" . $emptyMessage . "</p>";
    $bodyEmpty = 1;
  }
  if(($bodyEmpty === 0) || ($sendIfEmpty === 1)) {
    cooldown("set");
    echo $headers . "\n\n<html>
<head>
</head>
<body>

<p>Hi,</p>

<p>At <u>" . $now . "</u> the following DNS changes were detected:</p>

" . $body . "

<p>Thank you,<br/>CYC DNS Monitor</p>\n\n</body>
</html>\n";
  } else {
    echo $emptyMessage . "\n";
  }
}

function updateExpected($records) {
  file_put_contents("dns-records.json", json_encode($records));
  echo "Done.\n";
}

//Handle program arguments
switch($argv[1]) {
  case '':
    exit("No argument specified. Try 'report' or 'update'.\n");
  case "update":
    echo "Updating expected records...\n";
    updateExpected(getRecords($targets));
    break;
  case "report": {
    switch($argv[2]) {
      case '':
        exit("No argument specified for 'report'. Try: 'standard', 'debug'\n");
      case 'standard':
        cooldown("check");
        $to = "dns-alerts@york.gov.uk";
        $reportType = "";
        $sendIfEmpty = 0;
        $writeRecords = 1;
        generateReport($targets, $expected, $to, $reportType, $sendIfEmpty, $writeRecords);
        break;
      case 'debug':
        $to = "wst-ci@york.gov.uk";
        $reportType = "(Debug) ";
        $sendIfEmpty = 1;
        $writeRecords = 0;
        generateReport($targets, $expected, $to, $reportType, $sendIfEmpty, $writeRecords);
        break;
      default:
        exit("Invalid argument for 'report'.\n");
    }
    break;
  } default:
    exit("Invalid argument.\n");
} ?>
